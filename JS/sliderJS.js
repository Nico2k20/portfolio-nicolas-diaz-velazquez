const info_proyectos = [
     {
         titulo: "Slider Web" ,
         imgs: "./imgs/slider.png",
         url : "https://nico2k20.gitlab.io/diapositiva/",
         repositorio:"https://gitlab.com/Nico2k20/diapositiva.git"
         
     },
     {  titulo: "Tateti Web" ,
        imgs: "./imgs/tateti.png",
        url : "https://nico2k20.gitlab.io/tateti-web/",
        repositorio:"https://gitlab.com/Nico2k20/tateti-web.git"
    },
    {   titulo: "Calculadora Web" ,
        imgs: "./imgs/calculadora.png",
        url : "https://nico2k20.gitlab.io/calculadorajs/",
        repositorio:"https://gitlab.com/Nico2k20/calculadorajs.git"
    },
    {   titulo: "Quote Machine Web" ,
        imgs: "./imgs/quoteMachine.png",
        url : "https://codepen.io/NICOLAS2K19/details/PojJyor",
        
    },

]

let indexUrls = 0;



function cambiarProyecto(indice){
    indexUrls+= indice;

    if(indexUrls>info_proyectos.length-1||indexUrls<0)
        indexUrls=0;
 
    if(info_proyectos[indexUrls]["repositorio"])
        document.getElementById("boton-repositorio-js").style.display="block" 
    
    else
        document.getElementById("boton-repositorio-js").style.display="none" 

    cambiarDiapo()
    document.getElementById("proyecto-js").style.transition = "all 500ms";
    document.getElementById("proyecto-js").style.backgroundImage = "url('"+info_proyectos[indexUrls]["imgs"]+"')";
    document.getElementById("titulo-web").innerHTML = info_proyectos[indexUrls]["titulo"];
    document.getElementById("diapo"+(indexUrls+1)).style.backgroundColor = "white";
}

function redireccionarDemostracion(){
    window.open(info_proyectos[indexUrls]["url"]);
}

function redireccionarRepositorio(){
    window.open(info_proyectos[indexUrls]["repositorio"]);
}

function cambiarDiapo() {
   let diapos = document.querySelectorAll("div.diapos");

   for(let i =0;i<diapos.length;i++){
       diapos[i].style.backgroundColor="black"
   }
}

