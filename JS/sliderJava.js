const info_proyectos_java = [

    {
        titulo:"Conjunto Dominante Mínimo de un Grafo",
        imgs:"./imgs/conjuntoDomMinimo.png" ,
        repositorio: "https://gitlab.com/Nico2k20/tp-3-skynet.git",
        
    },
    {    titulo:"Generador de Árboles Minimos de un Grafo",
        imgs:"./imgs/generadorArbolMinimo.png" ,
        repositorio: "https://gitlab.com/estebanmerlos33/tp2-version-final-grupo-skynet.git",
    },
    {    titulo:"Juego 2048",
        imgs:"./imgs/juego2048.png" ,
        repositorio: "https://gitlab.com/estebanmerlos33/tp-1-p3-version-final.git",
    },
    {    titulo:"Tateti",
        imgs:"./imgs/tatetiJava.png" ,
        repositorio: "https://gitlab.com/Nico2k20/tateti.git",
    }

    
];


let indexUrlsJava = 0;

function cambiarProyectoJava(indice){
    indexUrlsJava+= indice;

    if(indexUrlsJava>info_proyectos_java.length-1||indexUrlsJava<0)
        indexUrlsJava=0;

    cambiarDiapoJava();
    
    document.getElementById("proyecto-java").style.transition = "all 500ms";
    document.getElementById("proyecto-java").style.backgroundImage = "url('"+info_proyectos_java[indexUrlsJava]["imgs"]+"')";
    document.getElementById("titulo-java").innerHTML = info_proyectos_java[indexUrlsJava]["titulo"];
    document.getElementById("diapoJava"+(indexUrlsJava+1)).style.backgroundColor = "white";
}





function redireccionarRepositorioJava(){
    window.open(info_proyectos_java[indexUrlsJava]["repositorio"]);
}

function cambiarDiapoJava() {
    let diapos = document.querySelectorAll("div.diapos-java");
 
    for(let i =0;i<diapos.length;i++){
        diapos[i].style.backgroundColor="black"
    }
 }

